package presentation

import domain.models.TriangleType
import domain.usecase.GetTriangleType

class View(private val getTriangleType: GetTriangleType) {
    companion object {
        const val SIMPLE = "Обычный треугольник."
        const val EQUILATERAL = "Равносторонний треугольник."
        const val ISOSCELES = "Равнобедренный треугольник."
        const val NOT_A_TRIANGLE = "Длина каждой стороны должна быть меньше суммы двух других."
    }

    fun start():Boolean {
        println(
            when (getTriangleType()) {
                TriangleType.SIMPLE -> SIMPLE
                TriangleType.EQUILATERAL -> EQUILATERAL
                TriangleType.ISOSCELES -> ISOSCELES
                TriangleType.NOT_A_TRIANGLE -> NOT_A_TRIANGLE
                else -> return false
            }
        )
        return true
    }
}