package domain.usecase

import domain.models.TriangleType
import domain.repository.TriangleRepository

class GetTriangleType(private val triangleRepository: TriangleRepository) {
    operator fun invoke(): TriangleType? {
        val triangle = triangleRepository.getTriangle()?:return null
        if (triangle.a == triangle.b && triangle.b == triangle.c) {
            return TriangleType.EQUILATERAL
        }
        if (triangle.a == triangle.b || triangle.b == triangle.c || triangle.a == triangle.c) {
            return TriangleType.ISOSCELES
        }
        if (triangle.a >= triangle.b + triangle.c || triangle.b >= triangle.a + triangle.c || triangle.c >= triangle.b + triangle.a) {
            return TriangleType.NOT_A_TRIANGLE
        }
        return TriangleType.SIMPLE
    }
}