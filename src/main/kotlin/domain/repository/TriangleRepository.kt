package domain.repository

import domain.models.Triangle

interface TriangleRepository {
    fun getTriangle():Triangle?
}