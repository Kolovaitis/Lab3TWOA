package domain.models

enum class TriangleType {
    EQUILATERAL, ISOSCELES, SIMPLE, NOT_A_TRIANGLE
}