package data.repository

import data.datasource.DataSource
import domain.models.Triangle
import domain.repository.TriangleRepository

class TriangleFromSidesRepository(private val datasource: DataSource) : TriangleRepository {
    override fun getTriangle(): Triangle? {
        val a = datasource.getSide(1)
        if (a == 0) return null
        val b = datasource.getSide(2)
        if (b == 0) return null
        val c = datasource.getSide(3)
        if (c == 0) return null
        return Triangle(a, b, c)
    }
}