package data.datasource

interface DataSource {
    fun getSide(sideNumber: Int):Int
}