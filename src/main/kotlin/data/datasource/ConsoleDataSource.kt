package data.datasource

class ConsoleDataSource : DataSource {
    companion object {
        const val enterSideMessage = "Введите %d сторону:"
        const val errorMessage = "Длина стороны должна быть натуральным числом в интервале [1; ${Int.MAX_VALUE}]."
    }

    override fun getSide(sideNumber: Int): Int {
        var side = getSideOrNull(sideNumber)
        while (side == null) {
            side = getSideOrNull(sideNumber)
        }
        return side
    }

    private fun getSideOrNull(sideNumber: Int): Int? {
        println(java.lang.String.format(enterSideMessage, sideNumber))
        val side = readLine()?.toIntOrNull()
        return if (side != null && side >= 0) {
            side
        } else {
            println(errorMessage)
            null
        }
    }
}