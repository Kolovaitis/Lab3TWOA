import data.datasource.ConsoleDataSource
import data.repository.TriangleFromSidesRepository
import domain.usecase.GetTriangleType
import presentation.View
import java.io.BufferedInputStream
import javax.sound.sampled.AudioSystem

open class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            startMusic()
            start()
        }

        fun startMusic() {
            val ais =
                AudioSystem.getAudioInputStream(BufferedInputStream(Application::class.java.getResourceAsStream("audio/song.wav")))
            val clip = AudioSystem.getClip()
            clip.open(ais)
            clip.framePosition = 0
            clip.start()
        }

        fun start() {
            val dataSource = ConsoleDataSource()
            val repository = TriangleFromSidesRepository(dataSource)
            val getTriangleType = GetTriangleType(repository)
            val view = View(getTriangleType)
            while (view.start());

        }
    }
}